$(document).ready(function(){
	$('.slider').slick({
		arrows:true,
		dots:false,
		slidesToShow:1,
		autoplay:false,
        adaptiveHeight: true,
		speed:1000,
		responsive:[
			{
				breakpoint: 768,
				settings: {
					slidesToShow:1
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:1
				}
			}
		]
	});
    $('.slider2').slick({
		arrows:true,
		dots:false,
		slidesToShow:7,
		autoplay:true,
        adaptiveHeight: false,
		speed:1000,
		responsive:[
            {
				breakpoint: 1300,
				settings: {
					slidesToShow:5
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow:3
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:2
				}
			}
		]
	});
});
